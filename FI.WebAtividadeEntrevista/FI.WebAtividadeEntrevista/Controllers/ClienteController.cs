﻿using FI.AtividadeEntrevista.BLL;
using WebAtividadeEntrevista.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FI.AtividadeEntrevista.DML;
using WebAtividadeEntrevista.Utils;

namespace WebAtividadeEntrevista.Controllers
{
    public class ClienteController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Incluir()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Incluir(ClienteModel model)
        {
            BoCliente bo = new BoCliente();
            BoBeneficiario ben = new BoBeneficiario();

            if (!this.ModelState.IsValid)
            {
                List<string> erros = (from item in ModelState.Values
                                      from error in item.Errors
                                      select error.ErrorMessage).ToList();

                Response.StatusCode = 400;
                return Json(string.Join(Environment.NewLine, erros));
            }
            else
            {
                if (!ValidarCPF.EstaValido(model.CPF))
                {
                    Response.StatusCode = 400;
                    return Json(string.Join(Environment.NewLine, $"CPF Inválido"));
                }

                if (bo.VerificarExistencia(model.CPF))
                {
                    Response.StatusCode = 400;
                    return Json(string.Join(Environment.NewLine, $"CPF já cadastrado"));
                }

                model.Id = bo.Incluir(new Cliente()
                {
                    CEP = model.CEP,
                    Cidade = model.Cidade,
                    Email = model.Email,
                    Estado = model.Estado,
                    Logradouro = model.Logradouro,
                    Nacionalidade = model.Nacionalidade,
                    Nome = model.Nome,
                    Sobrenome = model.Sobrenome,
                    Telefone = model.Telefone,
                    CPF = model.CPF
                });

                //Adicionar Item
                if (model.Beneficiarios != null)
                {
                    foreach (var item in model.Beneficiarios)
                    {
                        ben.Incluir(new Beneficiario()
                        {
                            Nome = item.Nome,
                            CPF = item.CPF,
                            IdCliente = model.Id
                        });
                    }
                }

                return Json("Cadastro efetuado com sucesso");
            }
        }

        [HttpPost]
        public JsonResult Alterar(ClienteModel model)
        {
            BoCliente bo = new BoCliente();
            BoBeneficiario ben = new BoBeneficiario();

            if (!this.ModelState.IsValid)
            {
                List<string> erros = (from item in ModelState.Values
                                      from error in item.Errors
                                      select error.ErrorMessage).ToList();

                Response.StatusCode = 400;
                return Json(string.Join(Environment.NewLine, erros));
            }
            else
            {
                if (!ValidarCPF.EstaValido(model.CPF))
                {
                    Response.StatusCode = 400;
                    return Json(string.Join(Environment.NewLine, $"CPF Inválido"));
                }

                bo.Alterar(new Cliente()
                {
                    Id = model.Id,
                    CEP = model.CEP,
                    Cidade = model.Cidade,
                    Email = model.Email,
                    Estado = model.Estado,
                    Logradouro = model.Logradouro,
                    Nacionalidade = model.Nacionalidade,
                    Nome = model.Nome,
                    Sobrenome = model.Sobrenome,
                    Telefone = model.Telefone,
                    CPF = model.CPF
                });

                //Itens a serem removidos
                var listaBeneficiarios = ben.ListarPorCliente(model.Id);
                foreach (var obj in listaBeneficiarios)
                {
                    if (model.Beneficiarios == null || model.Beneficiarios.FirstOrDefault(x => x.CPF == obj.CPF || x.Id == obj.Id) == null)
                    {
                        ben.Excluir(obj.Id);
                    }
                }

                //Adicionar e alterar itens
                if (model.Beneficiarios != null)
                {
                    foreach (var item in model.Beneficiarios)
                    {
                        if (item.Id == 0 && listaBeneficiarios.Count(x => x.CPF == item.CPF || x.Id == item.Id) == 0)
                        {
                            ben.Incluir(new Beneficiario()
                            {
                                Nome = item.Nome,
                                CPF = item.CPF,
                                IdCliente = item.IdCliente
                            });
                        }
                        else
                        {
                            var beneficiario = listaBeneficiarios.FirstOrDefault(x => x.CPF == item.CPF || x.Id == item.Id);
                            ben.Alterar(new Beneficiario()
                            {
                                Id = beneficiario != null ? beneficiario.Id : item.Id,
                                Nome = item.Nome,
                                CPF = item.CPF,
                                IdCliente = item.IdCliente
                            });
                        }
                    }
                }
                return Json("Cadastro alterado com sucesso");
            }
        }

        [HttpGet]
        public ActionResult Alterar(long id = 0)
        {
            Cliente cliente = new BoCliente().Consultar(id);
            List<BeneficiarioModel> beneficiarios = new BeneficiarioModel().Lista(new BoBeneficiario().ListarPorCliente(id));
            Models.ClienteModel model = null;

            if (cliente != null)
            {
                model = new ClienteModel()
                {
                    Id = cliente.Id,
                    CEP = cliente.CEP,
                    Cidade = cliente.Cidade,
                    Email = cliente.Email,
                    Estado = cliente.Estado,
                    Logradouro = cliente.Logradouro,
                    Nacionalidade = cliente.Nacionalidade,
                    Nome = cliente.Nome,
                    Sobrenome = cliente.Sobrenome,
                    Telefone = cliente.Telefone,
                    CPF = cliente.CPF,
                    Beneficiarios = beneficiarios
                };
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult ClienteList(int jtStartIndex = 0, int jtPageSize = 0, string jtSorting = null)
        {
            try
            {
                int qtd = 0;
                string campo = string.Empty;
                string crescente = string.Empty;
                string[] array = jtSorting.Split(' ');

                if (array.Length > 0)
                    campo = array[0];

                if (array.Length > 1)
                    crescente = array[1];

                List<Cliente> clientes = new BoCliente().Pesquisa(jtStartIndex, jtPageSize, campo, crescente.Equals("ASC", StringComparison.InvariantCultureIgnoreCase), out qtd);

                //Return result to jTable
                return Json(new { Result = "OK", Records = clientes, TotalRecordCount = qtd });
            }
            catch (Exception ex)
            {
                return Json(new { Result = "ERROR", Message = ex.Message });
            }
        }

        public PartialViewResult AbrirModalBeneficiario()
        {
            return PartialView("_ModalBeneficiario");
        }
    }
}