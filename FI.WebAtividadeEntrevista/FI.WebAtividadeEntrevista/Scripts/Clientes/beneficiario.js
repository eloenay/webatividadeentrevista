﻿$(function () {

    "use script";

    window.Beneficiario = window.Beneficiario || {};

    Beneficiario.CarregarEventos = () => {
        $(document).ready(function () {
            Beneficiario.Incluir();
            Beneficiario.Alterar();
            Beneficiario.Excluir();
        });
    };

    Beneficiario.MontarTabela = () => {
        if (listaBeneficiario) {
            $.each(listaBeneficiario, function (i, obj) {
                var tr = Beneficiario.MontarTr(obj.Nome, obj.CPF);
                $("#tb-beneficiario").append(tr);
            });
        }
    };

    Beneficiario.Incluir = () => {
        $(document).on("click", ".btn-incluir", function () {
            var nome = $("#nome-beneficiario").val();
            var cpf = $("#cpf-beneficiario").val();
            var beneficiarioExiste = listaBeneficiario.find((obj) => { return obj.CPF === cpf });
            if (!nome) return alert("Campo nome está vazio");
            if (!cpf) return alert("Campo cpf está vazio");
            if (beneficiarioExiste && !cpfParaAlterar) return alert("Beneficiário ja cadastrado");

            var idCliente = 0;
            if (obj)
                idCliente = obj.Id;

            var idBeneficiario = 0;
            if (cpfParaAlterar) {
                listaBeneficiario = listaBeneficiario.filter(function (obj) { return obj.CPF !== cpfParaAlterar });
                trParaRemover.remove();
                idBeneficiario = idParaAlterar;
                cpfParaAlterar = null;
                idParaAlterar = null;
            }

            listaBeneficiario.push({ Id: idBeneficiario, Nome: nome, CPF: cpf, IdCliente: idCliente });
            var tr = Beneficiario.MontarTr(nome, cpf);
            $("#tb-beneficiario").append(tr);
            Beneficiario.LimparCampos();
        });
    };

    var cpfParaAlterar = null;
    var trParaRemover = null;
    var idParaAlterar = null;
    Beneficiario.Alterar = () => {
        $(document).on("click", ".btn-alterar", function () {
            var cpf = $(this).data('cpf');
            var beneficiarioExiste = listaBeneficiario.find((obj) => { return obj.CPF === cpf });
            $("#nome-beneficiario").val(beneficiarioExiste.Nome);
            $("#cpf-beneficiario").val(beneficiarioExiste.CPF);
            cpfParaAlterar = beneficiarioExiste.CPF;
            idParaAlterar = beneficiarioExiste.Id;
            trParaRemover = $(this).closest('tr');
        });
    };

    Beneficiario.Excluir = () => {
        $(document).on("click", ".btn-excluir", function () {
            var cpf = $(this).data('cpf');
            listaBeneficiario = listaBeneficiario.filter(function (obj) { return obj.CPF !== cpf });
            $(this).closest('tr').remove();
        });
    };

    Beneficiario.LimparCampos = () => {
        $("#nome-beneficiario").val("");
        $("#cpf-beneficiario").val("");
    };

    Beneficiario.MontarTr = (nome, cpf) => {
        return `<tr>` +
            `<td>${cpf}</td>` +
            `<td>${nome}</td>` +
            '<td>' +
            `<button type="button" class="btn btn-sm btn-primary btn-alterar" data-cpf="${cpf}" style="margin-right:5px">Alterar</button>` +
            `<button type="button" class="btn btn-sm btn-primary btn-excluir" data-cpf="${cpf}">Excluir</button>` +
            '</td> ' +
            '</tr> ';
    };
});