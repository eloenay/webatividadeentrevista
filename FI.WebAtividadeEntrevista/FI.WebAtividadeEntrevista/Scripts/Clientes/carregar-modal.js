﻿$(function () {

    "use string";

    window.EventoModal = window.EventoModal || {};

    EventoModal.AbrirModal = function () {
        $(document).ready(function () {
            $(document).on("click", ".btn-abrirModal", function () {
                var carregarModal = $(".modal.abrir-modal");
                carregarModal.empty();
                var url = $(this).data("abrirmodal");
                $.ajax({
                    url: url,
                    method: "post",
                    success: function (result) {
                        carregarModal.append(result);
                        carregarModal.modal("show");
                        Mask.CarregarEventos();
                        Beneficiario.MontarTabela();
                        Beneficiario.CarregarEventos();
                    },
                    error: function () {
                        alert('Erro ao abrir informações');
                    }
                });
            });

            $(document).on("click", "#btn-fechar-modal", function () {
                $(".modal.abrir-modal").modal("hide");
            });
        });
    };

    EventoModal.AbrirModal();
});