﻿$(function () {

    "use string";

    window.Mask = window.Mask || {};

    Mask.CarregarEventos = () => {
        $(document).ready(function () {
            Mask.Values();
            Mask.Validate();
        });
    }

    Mask.Values = function () {
        $('#CEP').mask('00000-000');
        $('#Telefone').mask('(00) 0000-0000');
        $('.cpf').mask('000.000.000-00', { reverse: true });
    }

    Mask.Validate = function () {
        $(document).on('keyup', '.cpf', function () {
            var cpfField = $(this);
            var cpf = cpfField.val().replace(/[^\d]+/g, '');
            if (cpf == '') return false;
            if (cpf.length < 11) return false;
            if (cpf == "00000000000" || cpf == "11111111111" || cpf == "22222222222" || cpf == "33333333333" || cpf == "44444444444" || cpf == "55555555555" || cpf == "66666666666" || cpf == "77777777777" || cpf == "88888888888" || cpf == "99999999999") {
                $(".msg-error").html('<span for="Cpf" style="color:red">CPF inválido.</span>');
                $(".btn-incluir").attr("disabled", "disabled");
                return false;
            }
            add = 0;
            for (i = 0; i < 9; i++) {
                add += parseInt(cpf.charAt(i)) * (10 - i);
            } rev = 11 - add % 11;
            if (rev == 10 || rev == 11) rev = 0;
            if (rev != parseInt(cpf.charAt(9))) {
                $(".msg-error").html('<span for="Cpf" style="color:red">CPF inválido.</span>');
                $(".btn-incluir").attr("disabled", "disabled");
                return false;
            }
            add = 0;
            for (i = 0; i < 10; i++) {
                add += parseInt(cpf.charAt(i)) * (11 - i);
            } rev = 11 - add % 11;
            if (rev == 10 || rev == 11) rev = 0;
            if (rev != parseInt(cpf.charAt(10))) {
                $(".msg-error").html('<span for="Cpf" style="color:red">CPF inválido.</span>');
                $(".btn-incluir").attr("disabled", "disabled");
                return false;
            }
            $(".msg-error").empty();
            $(".btn-incluir").removeAttr("disabled")
            return true;
        });
    }

    Mask.CarregarEventos();
});