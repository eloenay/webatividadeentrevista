﻿using FI.AtividadeEntrevista.DML;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebAtividadeEntrevista.Models
{
    /// <summary>
    /// Classe de Modelo de Beneficiario
    /// </summary>
    public class BeneficiarioModel
    {
        public long Id { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        [Required]
        public string Nome { get; set; }

        /// <summary>
        /// CPF
        /// </summary>
        [Required]
        public string CPF { get; set; }

        /// <summary>
        /// IdCliente
        /// </summary>
        public long IdCliente { get; set; }

        public List<BeneficiarioModel> Lista(List<Beneficiario> list)
        {
            var models = new List<BeneficiarioModel>();
            foreach (var item in list)
            {
                models.Add(new BeneficiarioModel()
                {
                    Id = item.Id,
                    Nome = item.Nome,
                    CPF = item.CPF,
                    IdCliente = item.IdCliente
                });
            }
            return models;
        }
    }    
}